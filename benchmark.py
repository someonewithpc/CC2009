#!/usr/bin/env python3
import itertools
import os
import timeit
from random import choice
from string import ascii_letters as alphabet
from binascii import hexlify
from cryptography.hazmat.primitives.ciphers import Cipher, algorithms, modes
from cryptography.hazmat.primitives.asymmetric.rsa import generate_private_key
from cryptography.hazmat.primitives.asymmetric import padding
from cryptography.hazmat.primitives import hashes
from enum import Enum
from tabulate import tabulate

class Operation(Enum):
    encrypt = 'encrypt'
    decrypt = 'decrypt'

# --- AES ---
def aes(key, text, mode = Operation.encrypt):
    cipher = Cipher(algorithms.AES(key), modes.ECB())
    if mode == Operation.encrypt:
        encryptor = cipher.encryptor()
        padded_length = (len(text)//16 + 1) * 16
        pad_char = hex(padded_length - len(text))[2:].encode('utf-8') if len(text) % 16 != 0 else b'\0'
        return encryptor.update(text.ljust(padded_length, pad_char)) + encryptor.finalize()
    else:
        decryptor = cipher.decryptor()
        res = decryptor.update(text) + decryptor.finalize()
        padding = int(chr(res[-1]), 16) if res[-1] != 0 else 16
        return res[:-padding]

def test_aes():
    aes_key = os.urandom(256 // 8) # 256 bits
    assert b'CC2009 Assignment #1' == aes(aes_key, aes(aes_key, b'CC2009 Assignment #1', mode = Operation.encrypt), mode = Operation.decrypt)
test_aes()

# --- RSA ---
def rsa(pub_key, priv_key, text, mode = Operation.encrypt):
    pad = padding.OAEP(mgf = padding.MGF1(algorithm = hashes.SHA256()), algorithm = hashes.SHA256(), label = None)
    if mode == Operation.encrypt:
        return pub_key.encrypt(text, pad)
    else:
        return priv_key.decrypt(text, pad)

def test_rsa():
    rsa_priv_key = generate_private_key(public_exponent=65537, key_size=256 * 8) # 256 bytes
    rsa_pub_key = rsa_priv_key.public_key()
    assert b'CC2009 Assignment #1' == rsa(rsa_pub_key, rsa_priv_key, rsa(rsa_pub_key, rsa_priv_key, b'CC2009 Assignment #1', mode = Operation.encrypt), mode = Operation.decrypt)
test_rsa()

# --- SHA-256 ---
def sha256(text):
    digest = hashes.Hash(hashes.SHA256())
    digest.update(text)
    return digest.finalize()

def test_sha256():
    assert b"W\x88'\rP#\xa9\xd3\xda\xafg\xd8\x85\xb3\x1a\xda\x0c\x80\xdd8n3W\xe3\xb4w\xf35BIq\xc6" == sha256(b'CC2009 Assignment #1')
test_sha256()

# -- Create benchmark assets --

file_algo_sizes = {
    'AES':    [2**3, 2**6, 2**9, 2**12, 2**15, 2**18, 2**21],
    'SHA256': [2**3, 2**6, 2**9, 2**12, 2**15, 2**18, 2**21],
    'RSA':    [2**1, 2**2, 2**3, 2**4,  2**5,  2**6,  2**7],
}

os.makedirs('files', exist_ok=True)

for algo in file_algo_sizes:
    for size in file_algo_sizes[algo]:
        file_name = f"files/{algo}_{size}"
        if not os.path.exists(file_name):
            with open(file_name, 'w') as f:
                for _ in range(size):
                    f.write(choice(alphabet))

# -- Run Benchmark --

# Override timeit's template so it also returns the results from the function call
# see https://hg.python.org/cpython/file/3.5/Lib/timeit.py#l70
timeit.template = """
def inner(_it, _timer{init}):
    {setup}
    _t0 = _timer()
    for _i in _it:
        retval = {stmt}
    _t1 = _timer()
    return _t1 - _t0, retval
"""

def benchmark_step(fd, alg_name, size, func):
    # Timer.repeat returns [(time, result)], unzip it to ([time], [result])
    execution_time, result = zip(*timeit.Timer(func).repeat(
        repeat=num_repetitions,
        number=num_runs
    ))
    average_duration = sum(execution_time)/(num_repetitions*num_runs)
    table_results.append([alg_name, size, average_duration])
    fd.write(f'{size} {average_duration}\n')
    return result[0]

def benchmark(algo, forward_description, forward_filename, forward_operation, backward_description, backward_filename, backward_operation):
    forward_file = open(f'output/{forward_filename}', 'w')
    backward_file = open(f'output/{backward_filename}', 'w') if not backward_filename is None else None
    for size in file_algo_sizes[algo]:
        file_name = f"files/{algo}_{size}"
        with open(file_name, 'rb') as f:
            file_contents = f.read()
            ciphertext = benchmark_step(forward_file, forward_description, size, forward_operation(file_contents))
            if not backward_description is None:
                benchmark_step(backward_file, backward_description, size, backward_operation(ciphertext))
    forward_file.close()
    if not backward_file is None:
        backward_file.close()

num_runs = 10
num_repetitions = 3

aes_key = os.urandom(256 // 8) # 256 bits
rsa_priv_key = generate_private_key(public_exponent=65537, key_size=256 * 8) # 256 bytes
rsa_pub_key = rsa_priv_key.public_key()

os.makedirs('output', exist_ok=True)

table_results = []

benchmark(
    'AES',
    'AES Encrypt', 'bench_aes_encrypt.txt', lambda file_contents: lambda: aes(aes_key, file_contents, mode = Operation.encrypt),
    'AES Decrypt', 'bench_aes_decrypt.txt', lambda ciphertext: lambda: aes(aes_key, ciphertext, mode = Operation.decrypt)
)

benchmark(
    'RSA',
    'RSA Encrypt', 'bench_rsa_encrypt.txt', lambda file_contents: lambda: rsa(rsa_pub_key, rsa_priv_key, file_contents, mode = Operation.encrypt),
    'RSA Decrypt', 'bench_rsa_decrypt.txt', lambda ciphertext: lambda: rsa(rsa_pub_key, rsa_priv_key, ciphertext, mode = Operation.decrypt)
)

benchmark(
    'SHA256',
    'SHA256 Hash', 'bench_sha256.txt', lambda file_contents: lambda: sha256(file_contents),
    None, None, lambda x: None,
)

table_results.sort(key = lambda row: row[0])

print(tabulate(table_results, headers=['Algorithm', 'Size (bytes)', 'Average Duration (μs)']))
