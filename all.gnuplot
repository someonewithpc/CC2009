#!/usr/bin/gnuplot
set terminal png size 1200,900 enhanced font "Helvetica,20"

set autoscale
set grid
set style data linespoints
set ylabel "Time (µs)"
set xlabel "Size (bytes)"

set key outside

set output "output/plot_all.png";
plot "output/bench_aes_encrypt.txt" title "AES Encrypt" lw 4, \
     "output/bench_aes_decrypt.txt" title "AES Decrypt" lw 4, \
     "output/bench_rsa_encrypt.txt" title "RSA Encrypt" lw 4, \
     "output/bench_rsa_decrypt.txt" title "RSA Decrypt" lw 4, \
     "output/bench_sha256.txt"      title "SHA-256" lw 4

set output "output/plot_aes_sha256.png";
plot "output/bench_aes_encrypt.txt" title "AES Encrypt" lw 4, \
     "output/bench_aes_decrypt.txt" title "AES Decrypt" lw 4, \
     "output/bench_sha256.txt"      title "SHA-256" lw 4

set output "output/plot_rsa.png";
plot "output/bench_rsa_encrypt.txt" title "RSA Encrypt" lw 4, \
     "output/bench_rsa_decrypt.txt" title "RSA Decrypt" lw 4

set xrange [0:140]
set output "output/plot_aes_rsa.png";
plot "output/bench_aes_encrypt.txt" title "AES Encrypt" lw 4, \
     "output/bench_aes_decrypt.txt" title "AES Decrypt" lw 4, \
     "output/bench_rsa_encrypt.txt" title "RSA Encrypt" lw 4, \
     "output/bench_rsa_decrypt.txt" title "RSA Decrypt" lw 4
